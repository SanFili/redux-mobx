import { AxiosResponse } from "axios";
import { ApiDataType } from "types/lists";

import Api from "./Api";

export class UsersApi extends Api {
  getUsers(page: number): Promise<ApiDataType> {
    return this.axios
      .get(`https://reqres.in/api/users?page=${page}`)
      .then((response: AxiosResponse<ApiDataType>) => {
        return response.data;
      });
  }
}
