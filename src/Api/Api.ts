import axios, { AxiosInstance } from "axios";

const apiUrl = "https://reqres.in/api/users";

export default class Api {
  protected axios: AxiosInstance;

  constructor() {
    this.axios = axios.create({
      baseURL: apiUrl,
    });
  }
}
