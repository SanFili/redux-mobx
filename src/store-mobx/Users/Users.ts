import { usersInitial } from "Constants/initialState";
import { makeAutoObservable } from "mobx";
import { UserType } from "types/lists";

class UsersStore {
  users: UserType[] = usersInitial.users;

  currentPage: number = usersInitial.currentPage;

  totalPages: number = usersInitial.totalPages;

  totalUsers: number = usersInitial.totalUsers;

  usersPerPage: number = usersInitial.usersPerPage;

  constructor() {
    makeAutoObservable(this);
  }

  addUsers(users: UserType[]) {
    this.users = [...this.users, ...users];
  }

  setCurrentPage(currentPage: number) {
    this.currentPage = currentPage;
  }

  setTotalPages(totalPages: number) {
    this.totalPages = totalPages;
  }

  setTotalUsers(totalUsers: number) {
    this.totalUsers = totalUsers;
  }

  setUsersPerPage(usersPerPage: number) {
    this.usersPerPage = usersPerPage;
  }

  setTotals(totalPages: number, totalUsers: number, usersPerPage: number) {
    this.setTotalPages(totalPages);
    this.setTotalUsers(totalUsers);
    this.setUsersPerPage(usersPerPage);
  }
}

export default new UsersStore();
