import { UserType } from "./lists";

export type UsersStoreType = {
  users: UserType[];
  currentPage: number;
  totalPages: number;
  totalUsers: number;
  usersPerPage: number;
};

export type PayloadType = {
  payload: number | UserType[];
}[];
