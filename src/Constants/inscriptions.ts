export const mainPageLink = "На главную";

export const MobxPageLink = "Mobx";

export const ReduxPageLink = "Redux";

export const offlineButton = "Offline";

export const onlineButton = "Очистить localStorage";
