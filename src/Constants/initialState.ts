import { UsersStoreType } from "types/store";

export const usersInitial: UsersStoreType = {
  users: [],
  currentPage: 1,
  totalPages: 2,
  totalUsers: 12,
  usersPerPage: 6,
};
