import "./App.css";

import { MainPage } from "Components/MainPage";
import { Users } from "Components/Users";
import React from "react";
import { Provider } from "react-redux";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import store from "store";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Switch>
          <Route exact path="/">
            <MainPage />
          </Route>
          <Route path="/redux">
            <Provider store={store}>
              <Users />
            </Provider>
          </Route>
          <Route path="/mobx">
            <Users />
          </Route>
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
