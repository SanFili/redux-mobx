import "./User.css";

import React, { FC } from "react";
import { UserType } from "types/lists";

type UserProps = {
  userData: UserType;
};

export const User: FC<UserProps> = ({ userData }) => {
  return (
    <div className="user">
      <img className="user__avatar" src={userData.avatar} alt="avatar" />
      <div className="user__description">
        <h4>
          {userData.first_name} {userData.last_name}
        </h4>
        <p>e-mail: {userData.email}</p>
      </div>
    </div>
  );
};
