import "./Users.css";

import { UsersApi } from "Api";
import { usersInitial } from "Constants/initialState";
import {
  mainPageLink,
  offlineButton,
  onlineButton,
} from "Constants/inscriptions";
import { observer } from "mobx-react-lite";
import React, { FC, useCallback, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useLocation } from "react-router-dom";
import { RootStateType } from "store";
import {
  addUsersAction,
  setCurrentPageAction,
  setTotals,
} from "store/actions/Users";
import usersStore from "store-mobx/Users";
import { UserType } from "types/lists";
import { UsersStoreType } from "types/store";

import { User } from "./User";

const usersApi = new UsersApi();

const Users: FC = observer(() => {
  let location = useLocation();
  let path = location.pathname;
  let usersData = JSON.parse(localStorage.getItem("page")!);
  let mobxPage = path === "/mobx";

  const dispatch = !mobxPage ? useDispatch() : undefined;
  const { users, currentPage, totalPages, totalUsers, usersPerPage } = !mobxPage
    ? useSelector((state: RootStateType) => state.UsersReducer)
    : usersInitial;

  const [offlineUsersData, setOfflineUsersData] =
    useState<UsersStoreType>(usersInitial);
  let pageData = mobxPage
    ? {
        page: usersStore.currentPage,
        totalPages: usersStore.totalPages,
        usersPerPage: usersStore.usersPerPage,
        totalUsers: usersStore.totalUsers,
        users: usersStore.users,
      }
    : {
        page: currentPage,
        totalPages: totalPages,
        usersPerPage: usersPerPage,
        totalUsers: totalUsers,
        users: users,
      };

  useEffect(() => {
    if (pageData.users.length === 0) {
      if (!usersData) {
        if (mobxPage) {
          usersApi.getUsers(usersStore.currentPage).then((res) => {
            usersStore.setCurrentPage(res.page);
            usersStore.setTotals(res.total_pages, res.total, res.per_page);
            usersStore.addUsers(res.data);
          });
        } else {
          usersApi.getUsers(currentPage).then((res) => {
            dispatch!(setCurrentPageAction(res.page));
            dispatch!(setTotals(res.total_pages, res.total, res.per_page));
            dispatch!(addUsersAction(res.data));
          });
        }
      } else {
        if (mobxPage) {
          usersStore.addUsers(usersData.users);
          usersStore.setTotals(
            usersData.totalPages,
            usersData.totalUsers,
            usersData.usersPerPage
          );
        } else {
          dispatch!(addUsersAction(usersData.users));
          dispatch!(
            setTotals(
              usersData.totalPages,
              usersData.totalUsers,
              usersData.usersPerPage
            )
          );
        }
      }
    }
  }, []);

  useEffect(() => {
    if (
      !usersData &&
      pageData.page > 1 &&
      pageData.users.length < pageData.totalUsers
    ) {
      if (mobxPage) {
        usersApi.getUsers(pageData.page).then((res) => {
          usersStore.addUsers(res.data);
          usersStore.setCurrentPage(res.page);
        });
      } else {
        usersApi.getUsers(pageData.page).then((res) => {
          dispatch!(addUsersAction(res.data));
          dispatch!(setCurrentPageAction(res.page));
        });
      }
    }
  }, [pageData.page]);

  const prevPage = useCallback(() => {
    if (mobxPage) {
      usersStore.setCurrentPage(pageData.page - 1);
    } else {
      dispatch!(setCurrentPageAction(pageData.page - 1));
    }
  }, [pageData.page]);

  const nextPage = useCallback(() => {
    if (mobxPage) {
      usersStore.setCurrentPage(pageData.page + 1);
    } else {
      dispatch!(setCurrentPageAction(pageData.page + 1));
    }
  }, [pageData.page]);

  const getAllData = useCallback(() => {
    setOfflineUsersData({
      currentPage: pageData.page,
      totalPages: pageData.totalPages,
      totalUsers: pageData.totalUsers,
      usersPerPage: pageData.usersPerPage,
      users: pageData.users,
    });

    let savedPages = pageData.users.length / pageData.usersPerPage;
    for (let i = savedPages + 1; i <= pageData.totalPages; i++) {
      usersApi.getUsers(i).then((res) => {
        if (mobxPage) {
          usersStore.addUsers(res.data);
        } else {
          dispatch!(addUsersAction(res.data));
        }
      });

      setOfflineUsersData((prev) => {
        return {
          ...prev,
          users: [...prev.users, ...pageData.users],
        };
      });
    }

    alert("Данные успешно сохранены");
  }, [pageData.users]);

  useEffect(() => {
    if (offlineUsersData.users.length === offlineUsersData.totalUsers) {
      localStorage.setItem(
        "page",
        JSON.stringify({
          currentPage: offlineUsersData.currentPage,
          totalPages: offlineUsersData.totalPages,
          users: offlineUsersData.users,
          totalUsers: offlineUsersData.totalUsers,
          usersPerPage: offlineUsersData.usersPerPage,
        })
      );
    }
  }, [offlineUsersData]);

  const cleanOfflineUsersData = useCallback(() => {
    localStorage.removeItem("page");
    setOfflineUsersData(usersInitial);
    alert("Данные удалены");
  }, []);

  let renderUsers = pageData.users
    .slice(
      (pageData.page - 1) * pageData.usersPerPage,
      pageData.page * pageData.usersPerPage
    )
    .map((user: UserType) => <User key={user.id} userData={user} />);

  return (
    <div>
      <header className="users__header">
        <Link to="/">{mainPageLink}</Link>
      </header>
      <button className="users__offline" onClick={getAllData}>
        {offlineButton}
      </button>
      <button className="users__offline" onClick={cleanOfflineUsersData}>
        {onlineButton}
      </button>
      <div className="users__buttons">
        <button
          className="users__button"
          onClick={prevPage}
          disabled={pageData.page <= 1}
        >
          &#60;&#60;
        </button>
        <div>{pageData.page}</div>
        <button
          className="users__button"
          onClick={nextPage}
          disabled={pageData.page >= pageData.totalPages}
        >
          &#62;&#62;
        </button>
      </div>
      {renderUsers}
    </div>
  );
});

export default Users;
