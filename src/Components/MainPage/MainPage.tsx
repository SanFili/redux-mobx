import "./MainPage.css";

import { MobxPageLink, ReduxPageLink } from "Constants/inscriptions";
import React, { FC } from "react";
import { Link } from "react-router-dom";

const MainPage: FC = () => {
  return (
    <div className="main">
      <Link to="/redux">{ReduxPageLink}</Link>
      <Link to="/mobx">{MobxPageLink}</Link>
    </div>
  );
};

export default MainPage;
