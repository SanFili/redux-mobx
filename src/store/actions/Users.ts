import { Action, Dispatch } from "redux";
import {
  SET_CURRENT_PAGE,
  SET_TOTAL_PAGES,
  SET_TOTAL_USERS,
  SET_USERS,
  SET_USERS_PER_PAGE,
} from "store/Types";
import { UserType } from "types/lists";

import {
  addUsersActionType,
  setCurrentPageActionType,
  setTotalPagesActionType,
  setTotalUsersActionType,
  setUsersPerPageActionType,
} from "./types/Users";

export const addUsersAction = (users: UserType[]): addUsersActionType => ({
  type: SET_USERS,
  payload: users,
});

export const setCurrentPageAction = (
  currentPage: number
): setCurrentPageActionType => ({
  type: SET_CURRENT_PAGE,
  payload: currentPage,
});

export const setTotalPagesAction = (
  totalPages: number
): setTotalPagesActionType => ({
  type: SET_TOTAL_PAGES,
  payload: totalPages,
});

export const setTotalUsersAction = (
  totalUsers: number
): setTotalUsersActionType => ({
  type: SET_TOTAL_USERS,
  payload: totalUsers,
});

export const setUsersPerPageAction = (
  usersPerPage: number
): setUsersPerPageActionType => ({
  type: SET_USERS_PER_PAGE,
  payload: usersPerPage,
});

export const setTotals = (
  totalPages: number,
  totalUsers: number,
  usersPerPage: number
) => {
  return (dispatch: Dispatch<Action>) => {
    dispatch(setTotalPagesAction(totalPages));
    dispatch(setTotalUsersAction(totalUsers));
    dispatch(setUsersPerPageAction(usersPerPage));
  };
};
