import {
  SET_CURRENT_PAGE,
  SET_TOTAL_PAGES,
  SET_TOTAL_USERS,
  SET_USERS,
  SET_USERS_PER_PAGE,
} from "store/Types";
import { UserType } from "types/lists";

export type addUsersActionType = {
  type: typeof SET_USERS;
  payload: UserType[];
};

export type setCurrentPageActionType = {
  type: typeof SET_CURRENT_PAGE;
  payload: number;
};

export type setTotalPagesActionType = {
  type: typeof SET_TOTAL_PAGES;
  payload: number;
};

export type setTotalUsersActionType = {
  type: typeof SET_TOTAL_USERS;
  payload: number;
};

export type setUsersPerPageActionType = {
  type: typeof SET_USERS_PER_PAGE;
  payload: number;
};
