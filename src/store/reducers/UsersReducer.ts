import { usersInitial } from "Constants/initialState";
import { Action } from "redux-common-types-ts";
import {
  SET_CURRENT_PAGE,
  SET_TOTAL_PAGES,
  SET_TOTAL_USERS,
  SET_USERS,
  SET_USERS_PER_PAGE,
} from "store/Types";
import { PayloadType, UsersStoreType } from "types/store";

let initialState: UsersStoreType = usersInitial;

const UsersReducer = (state = initialState, action: Action<PayloadType>) => {
  switch (action.type) {
    case SET_USERS:
      return { ...state, users: [...state.users, ...action.payload] };
    case SET_CURRENT_PAGE:
      return { ...state, currentPage: action.payload };
    case SET_TOTAL_PAGES:
      return { ...state, totalPages: action.payload };
    case SET_TOTAL_USERS:
      return { ...state, totalUsers: action.payload };
    case SET_USERS_PER_PAGE:
      return { ...state, usersPerPage: action.payload };
    default:
      return state;
  }
};

export default UsersReducer;
